function a(n) {
    let count = 0;
    for(let i = 0; i < n; i++) {
        count += 1;
    }
    return count;
}

console.log(a(5));

const b = (n) => {
    let count = 0;
    for(let i = 0; i < 5*n; i++) {
        count += 1;
    }
    return count;
}

console.log(b(5));

function c(n) {
    let count = 0;
    for(let i = 0; i < n; i++) {
        count += 1;
    }
    count += 3;
    return count;
}

console.log(c(5));